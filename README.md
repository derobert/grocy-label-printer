Thermopolist
============

This is a program for printing Grocy labels to a Brother PT-P910BT label
printer over Bluetooth. It's currently a work-in-progress.

Install Notes
=============

Not final instructs. Just notes from setting up.

* `sdptool search SP` can find the printer
* `bluetoothctl` to pair it:
* - `scan bredr`
* - `pair MAC-ADDRESS`
* - `scan off`

* `apt install python3-venv python3-pil python3-pylibdmtx`
* `adduser --system --group --home /home/thermopolist --shell /bin/bash --disabled-password thermopolist`
* `adduser thermopolist bluetooth` — no idea if needed
* set up public key file to log in to that user, switch to that user
* `python3  -m venv --system-site-packages thermopolist-venv`
* `. thermopolist-venv/bin/activate`
* `pip install packbits`
* `mkdir src; cd src`
* `git clone -b initial 'https://gitlab.com/derobert/thermopolist.git`
* `git clone 'https://gitlab.com/ldo/python_fontconfig.git'`
* `pip install ./python_fontconfig/`

Some systemd unit files:

```systemd
# /etc/systemd/system/thermopolist.service
[Unit]
Description=print labels from Grocy

[Service]
Type=oneshot
User=thermopolist
TimeoutStartSec=3600
ExecStart=/home/thermopolist/thermopolist-venv/bin/python3 /home/thermopolist/src/thermopolist/src/thermopolist.py --address 30:05:5C:DD:84:9C --min-tape-size 12 --max-tape-size 12 --input-dir=/srv/thermopolist/ready

# /etc/systemd/system/thermopolist.path
[Unit]
Description=print Grocy labels when they exist
After=bluetooth.target

[Install]
WantedBy=paths.target

[Path]
DirectoryNotEmpty=/srv/thermopolist/ready
MakeDirectory=false
```
