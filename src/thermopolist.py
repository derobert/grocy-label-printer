#!/usr/bin/env python3

import argparse
import json
import re
import socket
import struct
import sys
import time
from collections import namedtuple
from pathlib import Path

import fontconfig
import packbits
from PIL import Image, ImageDraw, ImageFont, ImageOps
from pylibdmtx import pylibdmtx  # just had to be different

_PRINTER_RECV_SIZE = 256
_PRINTER_STATUS_SIZE = 32
_PRINTER_LINE_DOTS = 560  # aka bits, because b&w
_PRINTER_LINE_OCTETS = 70
_PRINTER_MIN_LEN_DOTS = 57

_BARCODE_SPACING = 15
_BARCODE_SIZE = 22


def _parse_args():
    parser = argparse.ArgumentParser(
        prog='Thermopolist',
        description='Send Grocy labels to a PT-910BT label maker')

    parser.add_argument('--dry-run', action='store_true', default=False)

    group = parser.add_argument_group(
        'Input options',
        '''Tell %(prog)s how to find and handle input files.''')
    group.add_argument('--input-dir',
                       required=True,
                       type=str,
                       metavar='DIRECTORY',
                       help='Directory to find JSON files in')
    group.add_argument(
        '--wait-time',
        required=False,
        default=.5,
        type=float,
        metavar='SECONDS',
        help=
        'Before exiting, wait this long and check for files again. [Default: 0.5]'
    )

    group = parser.add_argument_group(
        'Bluetooth options',
        '''Tell %(prog)s which printer to talk to. See the README for
        help setting these.''')
    group.add_argument('--address',
                       required=True,
                       type=str,
                       help='Bluetooth address (xx:xx:xx:xx:xx:xx)')
    group.add_argument('--channel',
                       required=False,
                       type=int,
                       default=2,
                       help='RFCOMM channel (default: %(default)i)')
    group.add_argument(
        '--connect-retries',
        required=False,
        type=int,
        default=34,
        metavar='RETRIES',
        help='Number of time to retry failed connection [default: 34]')
    group.add_argument('--connect-max-retry-time',
                       required=False,
                       type=int,
                       default=128,
                       metavar='SECONDS',
                       help='''
        Each retry waits twice as long; this is the max. Default
        settings give a bit over an hour total in wait time (excluding
        how long the connect attempts to fail) [Default: 128]
                       ''')

    group = parser.add_argument_group(
        'Tape size options',
        '''Set the minimum and maximum tape size (the height) in
        millimeters. This is to prevent wasting tape by printing labels
        that are either too small to be useful or larger than needed
        (and on expensive large tapes). If the wrong size tape is
        loaded, %(prog)s will exit without printing. Default is no
        restriction.''')
    group.add_argument('--min-tape-size',
                       required=False,
                       default=0,
                       type=int,
                       metavar='MILLIMETERS')
    group.add_argument(
        '--max-tape-size',
        required=False,
        default=1000,  # absurdly large; max is really 36mm
        type=int,
        metavar='MILLIMETERS')

    return parser.parse_args()


def _brother_init(sock):
    sock.send(b'\0' * 200)  # invalidate
    sock.send(b'\x1b\x40')  # initialize
    sock.send(b'\x1bia\x01')  # raster mode


def _brother_info(sock):
    sock.send(b'\x1biS')  # info
    dat = sock.recv(_PRINTER_RECV_SIZE)
    return _brother_info_decode(dat)


def _brother_info_decode(raw_data):
    info = namedtuple('_BrotherInfoRaw', [
        'print_head_mark', 'size', 'brother_code', 'series_code', 'model_code',
        'country_code', 'battery_level', 'extended_error', 'error_info_1',
        'error_info_2', 'media_width', 'media_type', 'num_of_colors', 'fonts',
        'japanese_fonts', 'mode', 'density', 'media_length', 'status_type',
        'phase_type', 'phase_number', 'notification_number',
        'expanion_area_size', 'tape_color_info', 'text_color_info'
    ])
    t = struct.unpack('>2B4c14BH4B6x', raw_data)
    return info(*t)


BROTHER_INCOMPATIBLE = 0xFF  # used for a bunch of fields
BROTHER_CLEANING_TAPE = 0xF0  # a tape & text color, apparently

_BARCODE_SIZES = (20, 22, 24, 26, 32, 36, 40)
# i = encode_datamatrix(bytes('grcy:p:5656:x65b992e301042', 'us-ascii'), size='20x20'); ii = Image.frombytes('RGB', (i.width, i.height), i.pixels); ii.reduce(5).convert(mode='1', dither=Dither.NONE).save('/tmp/foo.png')

# yapf: disable

_MEDIA_SIZE = {
    # dict maps length -> width -> tape info. Length and width are from
    # the info response.
    #
    # These values come from Table 2.3.2 and 2.3.5 in the Brother's
    # _Software Developer's Manual: Raster Command Reference
    # PT-P900/P900W/P950NW/P910BT Version 1.02_
    #
    #width in mm           sizes in dots
    #info  true   name      left  area
    0x00: (0,    'none',      0,    0,  ),
    0x04: (3.5,  '3.5 mm',  248,   48,  ),
    0x06: (6,    '6 mm',    240,   64,  ),
    0x09: (9,    '9 mm',    219,  106,  ),
    0x0C: (12,   '12 mm',   197,  150,  ),
    0x12: (18,   '18 mm',   155,  234,  ),
    0x18: (24,   '24 mm',   112,  320,  ),
    0x24: (36,   '36 mm',    45,  454,  ),
}
_MEDIA_TYPE = {
    # Table 4 of the info response on p. 27
    0x00: 'no media',
    0x01: 'laminated tape',
    0x03: 'non-laminated tape',
    0x04: 'fabric tape',
    0x11: 'heat-shrink tube (2:1)',
    0x13: 'fle tape',
    0x14: 'flexible ID tape',
    0x15: 'satin tape',
    0x17: 'heat-shrink tube (3:1)',
    0xFF: 'incompatible',
}
_MEDIA_COLOR = {
    # Table 8 and Table 9 of the info response on pp. 29–30. Appears the
    # text color and background colors use the same codes, so combined
    # in to one.
    0x00: 'n/a', # no media, not documented, from experiment
    0x01: 'white',
    0x02: 'other',
    0x03: 'clear',
    0x04: 'red',
    0x05: 'blue',
    0x06: 'yellow',
    0x07: 'green',
    0x08: 'black',
    0x09: 'clear',
    0x0a: 'gold',
    0x20: 'matte white',
    0x21: 'matte clear',
    0x22: 'matte silver',
    0x23: 'satin gold',
    0x24: 'satin silver',
    0x30: 'blue (d)', # used with white-on-blue
    0x31: 'red (d)', # used with white-on-red
    0x40: 'fluorescent orange',
    0x41: 'fluorescent yellow',
    0x50: 'berry pink (s)', # "simply stylish" ?
    0x51: 'light gray (s)',
    0x52: 'lime green (s)',
    0x60: 'yellow (f)', # fabic?
    0x61: 'pink (f)',
    0x62: 'blue (f)',
    0x70: 'white (heat-shrink tube)',
    0x90: 'white (flexible ID)',
    0x91: 'yellow (flexible ID)',
    0xF0: 'cleaning',
    0xF1: 'stencil',
    0xFF: 'incompatible',
}
_BATTERY_LEVEL = {
    # Table 10/PT-910BT. Just the last nibble.
    0: 'full',
    2: 'half-full',
    3: 'low',
    4: 'dead',
    7: 'not installed',
}
#yapf: enable


def _show_status(info):
    """Displays printer status information to stderr

    :info: _BrotherInfoRaw tuple
    :returns: Nothing

    """
    print(
        f"Media: {_MEDIA_SIZE[info.media_width][1]} {_MEDIA_COLOR[info.text_color_info]} text on {_MEDIA_COLOR[info.tape_color_info]} {_MEDIA_TYPE[info.media_type]}",
        file=sys.stderr)
    source = 'USB' if info.battery_level & 0x10 else 'battery'
    level = _BATTERY_LEVEL[info.battery_level & 0x0F]
    print(f"Power: from {source}, battery is {level}", file=sys.stderr)


def _check_status(info, min_tape_size, max_tape_size):
    """Confirms the printer status says we're good to print.

    :info: _BrotherInfoRaw tuple
    :min_tape_size: Minimum tape size (mm)
    :max_tape_size: Maximum tape size (mm)
    :returns: List of errors (empty if good to print)

    """
    errors = []
    size = _MEDIA_SIZE[info.media_width][0]

    # going to guess that if its incompatible, the rest of the info is
    # suspect (and irrelevant)
    if info.media_type == BROTHER_INCOMPATIBLE:
        errors.append("incompatible tape installed in printer")
    elif info.tape_color_info == BROTHER_INCOMPATIBLE:
        errors.append("incompatible tape color installed in printer")
    elif info.text_color_info == BROTHER_INCOMPATIBLE:
        errors.append("incompatible tape text color installed in printer")
    elif info.error_info_1 or info.error_info_2 or info.extended_error:
        # TODO: decode these for real
        errors.append(
            f"printer is indicating an error: {hex(info.extended_error)} {bin(info.error_info_1)} {bin(info.error_info_2)}"
        )
    elif 0 == size:
        errors.append("no tape installed in printer")
    else:
        if (info.tape_color_info == BROTHER_CLEANING_TAPE
                or info.text_color_info == BROTHER_CLEANING_TAPE):
            errors.append("printer has cleaning tape installed")
        if size < min_tape_size:
            errors.append(
                f"tape width {size} mm is less than minimum {min_tape_size} mm."
            )
        if size > max_tape_size:
            errors.append(
                f"tape width {size} mm is greater tham maximum {max_tape_size} mm."
            )
    return errors


def _setup_printer(sock, len_margin):
    """@todo: Sets up the label printer
    """
    sock.send(b'\x1BiM\x40')  # 0x40 = auto cut
    sock.send(b'\x1BiK\x04')  # 0x04 = half cut on, chain printing
    sock.send(b'\x1Bid' + struct.pack('<H', len_margin))
    sock.send(b'\x1BiA\x01')  # 0x01 = cut each label
    sock.send(b'M\x02')  # TIFF packbits compression
    sock.send(b'\x1Bi!\x00')  # enable automatic status


def _format_image(img_in, left_margin):
    img_bordered = Image.new(
        '1', (max(img_in.size[0], _PRINTER_MIN_LEN_DOTS), _PRINTER_LINE_DOTS),
        255)
    img_bordered.paste(img_in, (0, left_margin))
    return ImageOps.invert(img_bordered).transpose(
        Image.Transpose.ROTATE_90).transpose(
            Image.Transpose.FLIP_LEFT_RIGHT).tobytes(encoder_name='raw')


def _send_print_info(sock, media_type, media_width, raster_line_count,
                     which_page):
    PI_KIND = 0x02
    PI_WIDTH = 0x04
    PI_LENGTH = 0x08  # always giving 0...
    PI_QUALITY = 0x40  # not used
    PI_RECOVER = 0x80  # not used on P910-BT
    cmd = b'\x1Biz' + struct.pack('<4BIBx', PI_KIND | PI_WIDTH, media_type,
                                  media_width, 0, raster_line_count,
                                  which_page)
    sock.send(cmd)


def _send_raster(sock, img):
    ZERO = b'\0' * _PRINTER_LINE_OCTETS
    for i in range(0, len(img), _PRINTER_LINE_OCTETS):
        b = img[i:i + _PRINTER_LINE_OCTETS]
        if (b == ZERO):
            sock.send(b'Z')
        else:
            pb = packbits.encode(b)
            sock.send(b'G' + struct.pack('<H', len(pb)) + pb)
    sock.send(b'\x1A')


def _wait_print(sock):
    # we expect a phase change to printing, then page done, then
    # phase change to editing (not printing)
    printing = False
    print_done = False
    print('Waiting for print to finish...', file=sys.stderr)
    while printing or not print_done:
        buf = sock.recv(_PRINTER_STATUS_SIZE)
        info = _brother_info_decode(buf)
        # would use match, but yapf barfs on it ☹
        if 0x06 == info.status_type:
            printing = (0x01 == info.phase_type)
            print(f"phase change, printing={printing}", file=sys.stderr)
        elif 0x01 == info.status_type:
            print("printing is done", file=sys.stderr)
            print_done = True
        elif 0x02 == info.status_type:
            raise RuntimeError("Printer indicated an error", info)
        else:
            raise RuntimeError(f"Unhandled status type {info.status_change}",
                               info)


def _gen_barcode(characters, size_limit):
    # TODO: implement size_limit and doing variable size barcodes based
    #       on that (to handle multiple tape sizes)

    # the raw image has all kinds of WTF moments. Like, why 24-bit
    # color? And you can't set the barcode parameters: pixels per
    # element is fixed at 5, and you can't set the margin. So we have to
    # deal with all of it... or switch to a lower-level interface I
    # guess.
    sz = f'{_BARCODE_SIZE}x{_BARCODE_SIZE}'
    raw = pylibdmtx.encode(bytes(characters, 'us-ascii'), size=sz)
    # yapf: disable
    img = (Image.frombytes('RGB', (raw.width, raw.height), raw.pixels)
           .reduce(5)
           .convert(mode='1', dither=Image.Dither.NONE)
           .crop((2, 2, 2+_BARCODE_SIZE, 2+_BARCODE_SIZE))
           .resize((5*_BARCODE_SIZE,5*_BARCODE_SIZE)))
    # yapf: enable
    return img


def _find_font(family, style, pixel_size):
    """Return a file name for a font

    :family: Font family name
    :style: Font style (e.g., bold)
    :pixel_size: Size in pixels
    :returns: PIL ImageFont

    """
    conf = fontconfig.Config.get_current()
    query = f"{family}:style={style}:pixelsize={pixel_size}:dpi=360"
    pat = fontconfig.Pattern.name_parse(query)
    conf.substitute(pat, fontconfig.FC.MatchPattern)
    pat.default_substitute()
    found, status = conf.font_match(pat)
    file = found.get(fontconfig.PROP.FILE, 0)[0]
    return ImageFont.truetype(font=file, size=pixel_size)


def _gen_label(code, name, expiration, label_width):
    """Generates a label image

    :code: String. Grocy code.
    :name: String. Name of the item.
    :expiration: String or None. Expiration date & label
    :label_width: Width of the label tape print area in dots ("number of
                  print area pins" in §2.3.5
    :returns: PIL image

    """

    tmp_draw = ImageDraw.Draw(Image.new('1', (100, 100), color=255))
    bar_text_font = _find_font('DejaVu Sans Mono', 'book', 16)
    due_text_font = _find_font('DejaVu Sans', 'book', 24)
    name_text_font = _find_font('DejaVu Serif', 'bold', 36)

    barcode = _gen_barcode(code, 120)

    # Gotta figure out how long to make the label; first up, got to be
    # at least as long as the barcode. This is a little confusing as
    # what Brother calls the "width" everyone else would call the
    # height. It's the fixed dimension.
    label_length = barcode.width
    postbar_left = barcode.width + _BARCODE_SPACING

    bar_text_box = tmp_draw.textbbox((postbar_left, label_width),
                                     code,
                                     font=bar_text_font,
                                     anchor='ld')
    label_length = max(label_length, bar_text_box[2])

    if expiration is not None:
        due_text_box = tmp_draw.textbbox((postbar_left, 92),
                                         expiration,
                                         font=due_text_font,
                                         anchor='la')
        label_length = max(label_length, due_text_box[2])
    else:
        due_text_box = None

    # TODO: Replace this whole mess with, e.g. Pango (& Cairo?) someday
    #
    # The last one is fun; we need to make the label long enough to fit
    # the name in two lines. Start by splitting, capturing the
    # delimiter, then putting it back on. This isn't perfect (as e.g.,
    # as space at the end of a line can be dropped, but an em-dash can
    # not be).. but see TODO above.
    words = re.split('(\W+)', name)
    words.append('')
    words = [words[i] + words[i + 1] for i in range(0, len(words), 2)]
    lengths = [name_text_font.getlength(w) for w in words]
    print(words, lengths, file=sys.stderr)

    split = 0
    line1 = 0
    line2 = sum(lengths)
    target = line2 / 2
    while True:
        current = lengths[split]
        line2 -= current
        if line1 < line2:
            split += 1
            line1 += current
        else:
            line2 += current
            break
    print(f"split={split}, line1={line1}, line2={line2}", file=sys.stderr)

    twoline_name = '\n'.join([''.join(words[0:split]), ''.join(words[split:])])
    name_text_box = tmp_draw.multiline_textbbox((postbar_left, 0),
                                                twoline_name,
                                                font=name_text_font,
                                                anchor='la')
    label_length = max(label_length, name_text_box[2])

    print(
        f"bar_text_box: {bar_text_box}; due_text_box: {due_text_box}, name_text_box: {name_text_box}, label length: {label_length}",
        file=sys.stderr)

    barcode_height_remain = label_width - barcode.height

    # Now that we have the size, we can draw it...
    image = Image.new('1', (label_length, label_width), color=255)
    image.paste(barcode, (0, barcode_height_remain // 2))
    draw = ImageDraw.Draw(image)
    draw.text((postbar_left, label_width),
              code,
              font=bar_text_font,
              anchor='ld')
    if expiration is not None:
        draw.text((postbar_left, 92),
                  expiration,
                  font=due_text_font,
                  anchor='la')
    draw.multiline_text((postbar_left, 0),
                        twoline_name,
                        font=name_text_font,
                        anchor='la')

    image.save('/tmp/python.png')
    return image


def _connect(max_retries, max_wait, address, channel):
    retry = 0
    delay = 1
    while True:
        s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM,
                          socket.BTPROTO_RFCOMM)
        try:
            s.connect((address, channel))
            return s
        except OSError as e:
            print(
                f"Connect to {address} RFCOMM channel {channel}: {e}\nWill retry in {delay}s.",
                file=sys.stderr)
            s.close()
        ++retry
        time.sleep(delay)
        delay *= 2
        if retry > max_retries:
            raise ConnectionError("Out of retries; could not connect")


def main():
    args = _parse_args()
    print(args)

    s = _connect(max_retries=args.connect_retries,
                 max_wait=args.connect_max_retry_time,
                 address=args.address,
                 channel=args.channel)
    s.settimeout(60)
    _brother_init(s)
    info = _brother_info(s)
    print(info)
    assert info.print_head_mark == 0x80
    assert info.brother_code == b'B'
    assert info.series_code == b'0'
    assert info.model_code == b'x'
    _show_status(info)
    if errors := _check_status(info, args.min_tape_size, args.max_tape_size):
        print("Printer check found problems:",
              *[f"  - {s.capitalize()}" for s in errors],
              sep='\n',
              file=sys.stderr)
        exit(1)
    else:
        print("Printer check OK.", file=sys.stderr)

    while True:
        found_any = False
        for file in sorted(Path(args.input_dir).iterdir()):
            found_any = True
            with file.open() as fd:
                label_data = json.load(fd)
                print(label_data)
            label = _gen_label(code=label_data['grocycode'],
                               name=label_data['product'],
                               expiration=label_data.get('due_date'),
                               label_width=150)  # FIXME: hardcoded width

            img = _format_image(label, _MEDIA_SIZE[info.media_width][2])
            assert 0 == len(img) % _PRINTER_LINE_OCTETS
            if args.dry_run:
                found_any = False
                continue
            _setup_printer(s, 14)  # 14 is min per 2.3.3
            _send_print_info(s, 0, 12, len(img) // _PRINTER_LINE_OCTETS, 0)
            _send_raster(s, img)
            _wait_print(s)
            file.unlink()
        if found_any:
            time.sleep(args.wait_time)
        else:
            break

    s.close()  # though it doesn't matter since we're exiting


if __name__ == '__main__':
    main()
