#!/usr/bin/env python3

import os
import sys
import time
from http.server import BaseHTTPRequestHandler, HTTPServer

WRITE_PATH = '/srv/thermopolist/tmp'
READY_PATH = '/srv/thermopolist/ready'


class Receiver(BaseHTTPRequestHandler):
    """Receive JSON from Grocy"""

    def do_POST(self):
        if not self.headers['content-length']: return self._err_no_json()
        len = int(self.headers['content-length'], 10)
        if not len: return self._err_no_json()

        json = self.rfile.read(len)
        now = time.time_ns()
        pid = os.getpid()
        tmp = f"{WRITE_PATH}/{now}-{pid}.json"
        ready = f"{READY_PATH}/{now}-{pid}.json"

        with open(tmp, 'wb') as f:
            f.write(json)
        os.rename(tmp, ready)

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(b'{"status": "ok"}\n')

    def _err_no_json(self):
        self.send_response(500)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(b'Expected a posted JSON body\n')


def main():
    """Runs the server
    :returns: Nothing. Does not return.

    """
    server = HTTPServer(('', 8080), Receiver)
    server.serve_forever()


if __name__ == '__main__':
    main()
